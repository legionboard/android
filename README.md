vertretungsplan
==================

Dies ist eine Android-App für Vertretungspläne von Schulen. Sie basiert auf meiner [App für die Lornsenschule Schleswig](https://github.com/johan12345/ls-vertretungsplan), ist aber leicht auf andere Schulen erweiterbar. Weitere Informationen dazu im (leicht veralteten) [Wiki](https://github.com/johan12345/vertretungsplan/wiki/Schulen-hinzuf%C3%BCgen). Der Code des dazugehörigen [Servers](https://github.com/johan12345/vertretungsplan-server) und die [Schul-Konfigurationsdateien](https://github.com/johan12345/vertretungsplan-config-files) sind auch Open Source.

**Hinweis:** Ich bin im Moment dabei, eine komplett neu programmierte Version der App und des Servers zu entwickeln, die bis jetzt *nicht* Open Source ist und es möglicherweise auch nach dem Release nicht vollständig sein wird. Daher wird die alte App in diesem Git-Repository bis zum Release der neuen Version nur noch geringfügig und danach gar nicht mehr weiterentwickelt werden.

Die aktuell hier verfügbare App (die der Version im Play Store entspricht) läuft bereits mit einer neuen Version des Servers. Dieser ist ebenfalls nicht vollständig Open Source, nutzt aber die freie Bibliothek [substitution-schedule-parser](https://github.com/johan12345/substitution-schedule-parser). Wenn du eine neue Vertretungsplan-Software mit der App kompatibel machen möchtest, läuft das also über diese Bibliothek. Die letzte Version, die vollständig mit dem alten Server kompatibel ist, ist [2.0.7](https://github.com/johan12345/vertretungsplan/tree/7f711ee9e62911ce41bd6622810130641f21facc).

**English:** This is an Android app for substitution schedules of schools. It is based on the [app](https://github.com/johan12345/ls-vertretungsplan) I first created for Lornsenschule, Schleswig, Germany. Currently, it only supports some schools in Germany, but feel free to fork the [server code](https://github.com/johan12345/vertretungsplan-server) and add other schools.

**Note:** I am in the process of creating a completely rewritten version of the app which currently is *not* Open Source. It is possible that even after the release of the new version, only a part of the source code will be released. For this reason, only very minor changes will be made to the old app in this Git repository until the release of the new version, and no further development will take place afterwards.
